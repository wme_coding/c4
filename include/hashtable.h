#pragma once
const int tableSize = 100;

class HashTable{
    class Entry{
    public:
        int key;
        int value;

        Entry(int key, int value){
            this->key = key;
            this->value = value;
        }
    };

    Entry **pEntry;

public:
    HashTable();

    int hashFunction(int k);

    void insert(int k, int v);

    void remove(int k);

    int searchKey(int k);

    void clear();

    bool isEmpty();
};