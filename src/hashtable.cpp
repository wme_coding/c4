#include "../include/hashtable.h"

HashTable::HashTable() {
    pEntry = new Entry *[tableSize];
    clear();
}

int HashTable::hashFunction(int k) {
    return k%tableSize;
}

void HashTable::insert(int k, int v) {
    int h = hashFunction(k);
    while(pEntry[h] != nullptr && pEntry[h]->key != k){
        h = hashFunction(h + 1);
    }
    if(pEntry[h] != nullptr){
        delete pEntry[h];
    }
    pEntry[h] = new Entry(k, v);
}

void HashTable::remove(int k) {
    int h = hashFunction(k);
    while (pEntry[h] != nullptr) {
        if (pEntry[h]->key == k)
            break;
        h = hashFunction(h + 1);
    }
    if (pEntry[h] == nullptr) {
        //No Element found at key
        return;
    } else {
        delete pEntry[h];
    }
}

int HashTable::searchKey(int k) {
    int h = hashFunction(k);
    while (pEntry[h] != nullptr && pEntry[h]->key != k) {
        h = hashFunction(h + 1);
    }
    if (pEntry[h] == nullptr)
        return -1;
    else
        return pEntry[h]->value;
}

void HashTable::clear() {
    for(int i = 0; i < tableSize; i++){
        pEntry[i] = nullptr;
    }
}

bool HashTable::isEmpty() {
    for(int i = 0; i < tableSize; i++){
        if(pEntry[i] != nullptr){
            return false;
        }
    }
    return true;
}




